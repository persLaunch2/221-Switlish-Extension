import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import axios from 'axios'

Vue.use(VueRouter);

export default new VueRouter({
  routes
});
