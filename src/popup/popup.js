import Vue from "vue";
import App from "./App.vue";
import store from "./../store";
import router from "./router";
import Icon from "vue-svg-icon/Icon.vue";

Vue.component("icon", Icon);

new Vue({
  el: "#app",
  store,
  router,
  render: h => h(App)
});
